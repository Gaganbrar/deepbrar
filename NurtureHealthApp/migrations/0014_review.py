# Generated by Django 3.0.4 on 2020-05-24 08:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('NurtureHealthApp', '0013_order'),
    ]

    operations = [
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('cover_pic', models.FileField(upload_to='media/%y/%m/%d')),
                ('description', models.TextField()),
                ('added_on', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
